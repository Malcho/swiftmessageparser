﻿namespace Models
{
    public class SwiftMT799Message
    {
        public string? Block1 { get; set; }
        public string? Block2 { get; set; }
        public string? Block3 { get; set; }
        public string? Block4 { get; set; }
        public string? Block5 { get; set; }
        public string TransactionReferenceNumber { get; set; }
        public string? RelatedReference { get; set; }
        public string Narrative { get; set; }
    }
}
