﻿namespace Models
{
    public class FieldDefinitions
    {
        public string Tag { get; set; }
        public string FieldName { get; set; }
    }
}
