﻿using Models;
namespace Configurations
{
    public class Config
    {
        public IEnumerable<FieldDefinitions> FieldDefinitions { get; } = new[]
        {
            new FieldDefinitions { Tag = ":20:", FieldName = "TransactionReferenceNumber" },
            new FieldDefinitions { Tag = ":21:", FieldName = "RelatedReference" },
            new FieldDefinitions { Tag = ":79:", FieldName = "Narrative" }
        };

        public HashSet<string> MandatoryFields { get; } 
            = new HashSet<string> { "TransactionReferenceNumber", "Narrative" };

        public HashSet<string> FieldsForValidate { get; }
            = new HashSet<string> { "TransactionReferenceNumber", "RelatedReference" };
    }
}