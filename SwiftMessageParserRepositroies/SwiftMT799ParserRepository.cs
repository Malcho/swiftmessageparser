﻿using Contracts;
using Dapper;
using Helpers;
using Models;

namespace SwiftMessageParserRepositroies
{
    public class SwiftMT799ParserRepository : ISwiftMT799ParserRepository
    {
        private DapperContext _dapperContext;

        public SwiftMT799ParserRepository(DapperContext dapperContext)
        {
            _dapperContext = dapperContext;
        }

        public async Task SaveSwiftMessage(SwiftMT799Message message)
        {
            using var connection = _dapperContext.CreateConnection();
            var sql = @"
                INSERT INTO SwiftMT799Messages (
                    Block1,
                    Block2,
                    Block3,
                    Block4,
                    Block5,
                    TransactionReferenceNumber,
                    RelatedReference,
                    Narrative
                 
                )
                VALUES (
                    @Block1,
                    @Block2,
                    @Block3,
                    @Block4,
                    @Block5,
                    @TransactionReferenceNumber,
                    @RelatedReference,
                    @Narrative
                );";
            await connection.ExecuteAsync(sql, message);
        }
    }
}
