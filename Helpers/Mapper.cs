﻿using Models;
using Contracts;

namespace Helpers
{
    public class Mapper : IMapper
    {
        public SwiftMT799Message MapToSwiftMT799Message(Dictionary<string, string> blocks)
        {
            var swiftMT799Message = new SwiftMT799Message();

            if (blocks.ContainsKey("Block1"))
                swiftMT799Message.Block1 = blocks["Block1"];

            if (blocks.ContainsKey("Block2"))
                swiftMT799Message.Block2 = blocks["Block2"];

            if (blocks.ContainsKey("Block3"))
                swiftMT799Message.Block3 = blocks["Block3"];

            if (blocks.ContainsKey("Block4"))
                swiftMT799Message.Block4 = blocks["Block4"];

            if (blocks.ContainsKey("Block5"))
                swiftMT799Message.Block5 = blocks["Block5"];

            if(blocks.ContainsKey("TransactionReferenceNumber"))
                swiftMT799Message.TransactionReferenceNumber = blocks["TransactionReferenceNumber"];

            if(blocks.ContainsKey("RelatedReference"))
                swiftMT799Message.RelatedReference = blocks["RelatedReference"];

            if(blocks.ContainsKey("Narrative"))
                swiftMT799Message.Narrative = blocks["Narrative"];

            return swiftMT799Message;
        }
    }
}



