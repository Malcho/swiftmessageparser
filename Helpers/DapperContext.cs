﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Helpers
{
    public class DapperContext
    {
        protected readonly IConfiguration Configuration;

        public DapperContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IDbConnection CreateConnection()
        {
            return new SqliteConnection(Configuration.GetConnectionString("WebApiDatabase"));
        }

        public async Task Init()
        {
            // create database tables if they don't exist
            using var connection = CreateConnection();
            await _initUsers();

            async Task _initUsers()
            {
                var sql = """
                CREATE TABLE IF NOT EXISTS 
                SwiftMT799Messages (
                    Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    Block1 TEXT,
                    Block2 TEXT,
                    Block3 TEXT,
                    Block4 TEXT,
                    Block5 TEXT,
                    TransactionReferenceNumber TEXT,
                    RelatedReference TEXT,
                    Narrative TEXT
                );
            """;
                await connection.ExecuteAsync(sql);
            }
        }
    }
}


