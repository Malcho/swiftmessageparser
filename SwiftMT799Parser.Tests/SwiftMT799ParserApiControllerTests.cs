﻿using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SwiftMessageParserApi.Controllers;
using System.Text;

[TestFixture]
public class SwiftMT799ParserApiControllerTests
{
    private Mock<ISwiftMT799ParserService> _mockService;
    private SwiftMT799ParserApiController _controller;

    [SetUp]
    public void SetUp()
    {
        _mockService = new Mock<ISwiftMT799ParserService>();
        _controller = new SwiftMT799ParserApiController(_mockService.Object);
    }

    [Test]
    public async Task UploadSwiftFile_ReturnsBadRequest_IfFileIsNull()
    {
        // Arrange & Act
        var result = await _controller.UploadSwiftFile(null);

        // Assert
        Assert.IsInstanceOf<BadRequestObjectResult>(result);
    }

    [Test]
    public async Task UploadSwiftFile_ReturnsBadRequest_IfFileIsEmpty()
    {
        var mockFile = new Mock<IFormFile>();

        // Arrange & Act
        var result = await _controller.UploadSwiftFile(mockFile.Object);

        // Assert
        Assert.IsInstanceOf<BadRequestObjectResult>(result);
    }

    [Test]
    public async Task UploadSwiftFile_ReturnsOk_WithValidFile()
    {
        var validMessageContent = @"{1:F01PRCBBGSFAXXX1111111111}{2:O7991111111111ABGRSWACAXXX11111111111111111111N}{4:
:20:67-C111111-KNTRL 
:21:30-111-1111111
:79:NA VNIMANIETO NA: OTDEL BANKOVI GARANTSII
.
OTNOSNO: POTVARJDENIE NA AVTENTICHNOST NA
         PRIDRUJITELNO PISMO KAM ISKANE ZA
         PLASHTANE PO BANKOVA GARANCIA
.
UVAJAEMI KOLEGI,
.
UVEDOMJAVAME VI, CHE IZPRASHTAME ISKANE ZA 
PLASHTANE NA STOYNOST BGN 3.100,00, PREDSTAVENO 
OT NASHIA KLIENT.
.
S NASTOYASHTOTO POTVARZHDAVAME AVTENTICHNOSTTA NA 
PODPISITE VARHU PISMOTO NI, I CHE TEZI LICA SA 
UPALNOMOSHTENI DA PODPISVAT TAKAV DOKUMENT OT 
IMETO NA BANKATA AD.
.
POZDRAVI,
TARGOVSKO FINANSIRANE
-}{5:{MAC:00000000}{CHK:111111111111}}";

        var stream = new MemoryStream(Encoding.UTF8.GetBytes(validMessageContent));
        var mockFile = new Mock<IFormFile>();

        // Настройка на mock файла
        mockFile.Setup(f => f.Length).Returns(stream.Length);
        mockFile.Setup(f => f.OpenReadStream()).Returns(stream);

        // Настройка на mock услугата
        _mockService.Setup(s => s.ProcessMessage(validMessageContent)).Returns(Task.CompletedTask);

        // Act
        var result = await _controller.UploadSwiftFile(mockFile.Object);

        // Assert
        Assert.IsInstanceOf<OkResult>(result);
    }



}
