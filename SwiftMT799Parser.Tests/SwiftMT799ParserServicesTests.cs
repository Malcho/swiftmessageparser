using Helpers;
using SwiftMessageParserServices;
using Moq;
using Contracts;

[TestFixture]
public class SwiftMT799ParseProgramTests
{
    private SwiftMT799ParserService _swiftMT799ParserService;
    private Mock<ISwiftMT799ParserRepository> _mockRepository;  // ����� �� �����������
    private Mock<IMapper> _mockMapper;  // ����� �� ������

    [SetUp]
    public void Setup()
    {
        _mockRepository = new Mock<ISwiftMT799ParserRepository>();
        _mockMapper = new Mock<IMapper>();

        _mockRepository.Setup(repo => repo.SaveSwiftMessage(It.IsAny<Models.SwiftMT799Message>()))
                       .Returns(Task.CompletedTask);
        _mockMapper.Setup(m => m.MapToSwiftMT799Message(It.IsAny<Dictionary<string, string>>()))
                   .Returns(new Models.SwiftMT799Message()); 

        _swiftMT799ParserService = new SwiftMT799ParserService(_mockRepository.Object, _mockMapper.Object);
    }

    [Test]
    public async Task ProcessMessage_ThrowsAppException_WhenMandatoryFieldsAreMissing()
    {
        // Arrange
        var message = @"{1:F01DEUTDEFFAXXX0000000000}{2:I799BICBDEFX0XXXXN}{3:{108:2d8b7a3a-1d40-4ee9-bbfd-75ab78afc910}{121:1}{103:A}}{4:
:{177:
{451:0}
{445:/14979123450101234567890}}}";

        // Act & Assert
        Assert.ThrowsAsync<AppException>(() => _swiftMT799ParserService.ProcessMessage(message));
    }
    [Test]
    public async Task ProcessMessage_ThrowsAppException_WhenUsedInvalidSymbols()
    {
        // Arrange
        var message = @"{1:F01PRCBBGSFAXXX1111111111}{2:O7991111111111ABGRSWACAXXX11111111111111111111N}{4:
:20://67-C111111-KNTRL 
:21:30-111-1111111
:79:NA VNIMANIETO NA: OTDEL BANKOVI GARANTSII
.
OTNOSNO: POTVARJDENIE NA AVTENTICHNOST NA
         PRIDRUJITELNO PISMO KAM ISKANE ZA
         PLASHTANE PO BANKOVA GARANCIA
.
UVAJAEMI KOLEGI,
.
UVEDOMJAVAME VI, CHE IZPRASHTAME ISKANE ZA 
PLASHTANE NA STOYNOST BGN 3.100,00, PREDSTAVENO 
OT NASHIA KLIENT.
.
S NASTOYASHTOTO POTVARZHDAVAME AVTENTICHNOSTTA NA 
PODPISITE VARHU PISMOTO NI, I CHE TEZI LICA SA 
UPALNOMOSHTENI DA PODPISVAT TAKAV DOKUMENT OT 
IMETO NA BANKATA AD.
.
POZDRAVI,
TARGOVSKO FINANSIRANE
-}{5:{MAC:00000000}{CHK:111111111111}}";

        // Act & Assert
        Assert.ThrowsAsync<AppException>(() => _swiftMT799ParserService.ProcessMessage(message));
    }
    [Test]
    public async Task ProcessMessage_PassesSuccessfully_WithRequiredTags()
    {
        // Arrange
        var message = @"{1:F01PRCBBGSFAXXX1111111111}{2:O7991111111111ABGRSWACAXXX11111111111111111111N}{4:
:20:67-C111111-KNTRL 
:21:30-111-1111111
:79:NA VNIMANIETO NA: OTDEL BANKOVI GARANTSII
.
OTNOSNO: POTVARJDENIE NA AVTENTICHNOST NA
         PRIDRUJITELNO PISMO KAM ISKANE ZA
         PLASHTANE PO BANKOVA GARANCIA
.
UVAJAEMI KOLEGI,
.
UVEDOMJAVAME VI, CHE IZPRASHTAME ISKANE ZA 
PLASHTANE NA STOYNOST BGN 3.100,00, PREDSTAVENO 
OT NASHIA KLIENT.
.
S NASTOYASHTOTO POTVARZHDAVAME AVTENTICHNOSTTA NA 
PODPISITE VARHU PISMOTO NI, I CHE TEZI LICA SA 
UPALNOMOSHTENI DA PODPISVAT TAKAV DOKUMENT OT 
IMETO NA BANKATA AD.
.
POZDRAVI,
TARGOVSKO FINANSIRANE
-}{5:{MAC:00000000}{CHK:111111111111}}"; // ������ ��������� ���� �����

        // Act & Assert
       
            await _swiftMT799ParserService.ProcessMessage(message);
      
    }
}


