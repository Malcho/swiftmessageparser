﻿namespace Contracts
{
    public interface ISwiftMT799ParserService
    {
        Task ProcessMessage(string message);
    }
}
