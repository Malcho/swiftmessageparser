﻿using Models;
namespace Contracts
{
    public interface ISwiftMT799ParserRepository
    {
        Task SaveSwiftMessage(SwiftMT799Message message);
    }
}
