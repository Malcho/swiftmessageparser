﻿using Models;
namespace Contracts
{
    public interface IMapper
    {
        SwiftMT799Message MapToSwiftMT799Message(Dictionary<string, string> blocks);
    }
}
