﻿using Contracts;
using Microsoft.AspNetCore.Mvc;

namespace SwiftMessageParserApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SwiftMT799ParserApiController : ControllerBase
    {
        private readonly ISwiftMT799ParserService _swiftMessageParserService;

        public SwiftMT799ParserApiController(ISwiftMT799ParserService swiftMessageParserService)
        {
            _swiftMessageParserService = swiftMessageParserService;
        }

        [HttpPost("UploadSwiftFile")]
        public async Task<IActionResult> UploadSwiftFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("Invalid file.");
            }

            using var memoryStream = new MemoryStream();
            await file.CopyToAsync(memoryStream);
            var content = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());

            await _swiftMessageParserService.ProcessMessage(content);

            return Ok();
        }
    }
}
