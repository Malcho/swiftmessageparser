using Contracts;
using Helpers;
using SwiftMessageParserRepositroies;
using SwiftMessageParserServices;
using Serilog;
using Serilog.AspNetCore;

var builder = WebApplication.CreateBuilder(args);
Log.Logger = new LoggerConfiguration()
    .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day)
    .Enrich.FromLogContext()
    .CreateLogger();

builder.Services.AddSingleton(Log.Logger);
builder.Services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog());

builder.Services.AddHttpContextAccessor();
builder.Services.AddSingleton<Serilog.Extensions.Hosting.DiagnosticContext>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<DapperContext>();
builder.Services.AddScoped<ISwiftMT799ParserRepository, SwiftMT799ParserRepository>();
builder.Services.AddScoped<ISwiftMT799ParserService, SwiftMT799ParserService>();
builder.Services.AddScoped<IMapper, Mapper>();

var app = builder.Build();

using var scope = app.Services.CreateScope();
var context = scope.ServiceProvider.GetRequiredService<DapperContext>();
await context.Init();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.MapControllers();
app.UseMiddleware<ErrorHandlerMiddleware>();
app.UseSerilogRequestLogging();
app.Run();
