﻿using Configurations;
using Contracts;
using Helpers;
using Models;
using System.Text;

namespace SwiftMessageParserServices
{
    public class SwiftMT799ParserService : ISwiftMT799ParserService
    {
        private readonly ISwiftMT799ParserRepository _swiftMessageParserRepository;
        private readonly IMapper _mapper;

        public SwiftMT799ParserService(ISwiftMT799ParserRepository swiftMessageParserRepository,
                                        IMapper mapper)
        {
            _swiftMessageParserRepository = swiftMessageParserRepository;
            _mapper = mapper;
        }

        public async Task ProcessMessage(string message)
        {
            // Извличане на блоковете от съобщението
            var messageBlocks = ExtractBlocksFromMessage(message);

            // Обработка и валидация на блоковете
            var blocksAndFields = ExtractFieldsFromBlocks(messageBlocks);

            var mappedBlocksAndFields = _mapper.MapToSwiftMT799Message(blocksAndFields);

            await _swiftMessageParserRepository.SaveSwiftMessage(mappedBlocksAndFields);
        }

        private Dictionary<string, string> ExtractBlocksFromMessage(string message)
        {
            Stack<char> brackets = new Stack<char>();
            StringBuilder currentBlock = new StringBuilder();
            Dictionary<string, string> blocks = new Dictionary<string, string>();

            for (int i = 0; i < message.Length; i++)
            {
                char currentChar = message[i];

                if (currentChar == '{')
                    brackets.Push(currentChar);

                if (brackets.Count > 0)
                    currentBlock.Append(currentChar);


                if (currentChar == '}')
                {
                    brackets.Pop();

                    if (brackets.Count == 0)
                    {
                        string block = currentBlock.ToString();
                        string blockType = block.Substring(1, block.IndexOf(":") - 1);
                        blocks["Block" + blockType] = block;

                        currentBlock.Clear();
                    }
                }
            }
            return blocks;
        }

        private Dictionary<string, string> ExtractFieldsFromBlocks(Dictionary<string, string> blocks)
        {
            // Зареждане на конфигурацията за полетата
            var (fieldDefinitions, mandatoryFields, fieldsForValidate) = LoadFieldConfigurations();

            // Извличане на полетата от блоковете
            Dictionary<string, string> extractedFieldsFromBlocks = ExtractFieldsFromBlocks(blocks,
                                                                                        fieldDefinitions,
                                                                                        mandatoryFields);
            // Валидация на извлечените полета
            ValidateFields(extractedFieldsFromBlocks, blocks, fieldsForValidate);

            ThrowWhenMandatoryFieldsMissing(mandatoryFields); 

            return blocks;
        }      

        private Dictionary<string, string> ExtractFieldsFromBlocks(Dictionary<string, string> blocks,
                                                                List<FieldDefinitions> fieldDefinitions,
                                                                List<string> mandatoryFields)
        {
            // Структури за съхранение на откритите тагове и извлечените полета
            Dictionary<string, string> extractedFieldsFromBlocks = new Dictionary<string, string>();
            Dictionary<int, FieldDefinitions> discoveredTags = new Dictionary<int, FieldDefinitions>();

            // Анализиране и извличане на полетата от всеки блок
            foreach (var currentBlock in blocks.ToList())
            {
                bool isBlockParsed = ExtractFieldsFromSingleBlock(currentBlock, fieldDefinitions,
                                                                 discoveredTags,
                                                                 mandatoryFields);

                if (isBlockParsed)
                {
                    ExtractAllFieldsFromBlock(currentBlock.Value, discoveredTags, extractedFieldsFromBlocks);
                    
                    // Добавяне на полета към основната структура
                    AddExtractedFieldsToBlocks(extractedFieldsFromBlocks, blocks);   
                    
                    DeleteParsedBlock(currentBlock.Key, blocks);

                    extractedFieldsFromBlocks.Clear();
                }
            }

            return extractedFieldsFromBlocks;
        }

        private bool ExtractFieldsFromSingleBlock(KeyValuePair<string, string> block,
                                                                List<FieldDefinitions> fieldDefinitions,
                                                                Dictionary<int, FieldDefinitions> discoveredTags,
                                                                List<string> mandatoryFields)
        {
            bool isBlockParsed = false;

            // Търсим съвпадения между блоковете и дефинициите на полетата
            foreach (var fieldDef in fieldDefinitions)
            {
                if (block.Value.Contains($"{fieldDef.Tag}"))
                {
                    discoveredTags.Add(block.Value.IndexOf(fieldDef.Tag), new FieldDefinitions
                    {
                        FieldName = fieldDef.FieldName,
                        Tag = fieldDef.Tag
                    });
                    mandatoryFields.Remove(fieldDef.FieldName);
                    isBlockParsed = true;
                }
            }
            return isBlockParsed;
        }

        private void ExtractAllFieldsFromBlock(string blockValue, Dictionary<int, FieldDefinitions> discoveredTags,
                                                                Dictionary<string, string> extractedFieldsFromBlocks)
        {
            // Сортиране на таговете по позиция за последователна обработка
            var sortedTags = discoveredTags.OrderBy(x => x.Key).ToList();

            // Извличане на полетата между два последователни тага
            for (int i = 0; i < sortedTags.Count - 1; i++)
                ExtractFieldBetweenTags(blockValue,
                                        sortedTags[i],
                                        sortedTags[i + 1],
                                        extractedFieldsFromBlocks);

            // Обработка на последното поле в блока
            ExtractFinalFieldInBlock(blockValue, sortedTags.Last(),
                                        extractedFieldsFromBlocks);

        }

        private void ExtractFieldBetweenTags(string blockValue, KeyValuePair<int, FieldDefinitions> current,
                                     KeyValuePair<int, FieldDefinitions> next,
                                     Dictionary<string, string> extractedFieldsFromBlocks)
        {
            // Определяне на позициите за извличане на стойността между два тага
            int start = current.Key + current.Value.Tag.Length;
            int length = next.Key - start;
            string value = blockValue.Substring(start, length);

            SetFieldValue(extractedFieldsFromBlocks, current.Value.FieldName, value);
        }

        private void ExtractFinalFieldInBlock(string blockValue, KeyValuePair<int, FieldDefinitions> lastTag,
                                             Dictionary<string, string> extractedFieldsFromBlocks)
        {
            // Определяне на позицията и извличане на последното поле в блока
            int start = lastTag.Key + lastTag.Value.Tag.Length;
            int length = blockValue.Length - start - 1;
            string fieldValue = blockValue.Substring(start, length);

            SetFieldValue(extractedFieldsFromBlocks, lastTag.Value.FieldName, fieldValue);
        }

        private void SetFieldValue(Dictionary<string, string> fields, string fieldName, string value)
        {
            fields[fieldName] = value;
        }

        private void AddExtractedFieldsToBlocks(Dictionary<string, string> extractedFieldsFromBlocks,
                                                                Dictionary<string, string> blocks)
        {   
            // Добавяне на полета към основната структура
            foreach (var field in extractedFieldsFromBlocks)
                blocks[field.Key] = field.Value;
        }


        private void ValidateFields(Dictionary<string, string> extractedFieldsFromBlocks,
                                                                Dictionary<string, string> blocks,
                                                                List<string> fieldsForValidate)
        {
            // Проверка дали дадено поле трябва да бъде валидирано
            foreach (var field in extractedFieldsFromBlocks)
            {
                if (fieldsForValidate.Contains(field.Key))
                    ValidateFieldValue(field.Value, field.Key);
            }
        }

        private void ValidateFieldValue(string fieldValue, string fieldName)
        {
            if (fieldValue.StartsWith("/") || fieldValue.EndsWith("/") || fieldValue.Contains("//"))
                throw new AppException($"Error code: T26. Invalid format in field {fieldName}.");

        }

        private (List<FieldDefinitions>, List<string>, List<string>) LoadFieldConfigurations()
        {
            var fieldConfig = new Config();
            return (fieldConfig.FieldDefinitions.ToList(),
                    fieldConfig.MandatoryFields.ToList(),
                    fieldConfig.FieldsForValidate.ToList());
        }

        private void ThrowWhenMandatoryFieldsMissing(List<string> mandatoryFields)
        {
            if (mandatoryFields.Count > 0)
                throw new AppException($"Missing mandatory fields: {string.Join(", ", mandatoryFields)}");
        }

        private void DeleteParsedBlock(string blockKey, Dictionary<string, string> blocks)
        {
            blocks.Remove(blockKey);
        }
    }
}
